require('dotenv').config();
console.log(process.env.DISCORD_TOKEN);

//Load
const fs = require('fs');
const editJsonFile = require("edit-json-file");

//const { Calendar } = require("./calendar.js");
//console.log(Calendar.test());

// Require the necessary discord.js classes
const { Client, Intents, Message, MessageEmbed } = require('discord.js');
const client = new Client({ intents: ["GUILDS", "GUILD_MESSAGES", "DIRECT_MESSAGES"], partials: ["CHANNEL"] });
//const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
const token = process.env.DISCORD_TOKEN;
const PREFIX = "!";

//Json Files
const {startDate, monthData, campaign, holidays} = require("./calendars/Calendar.json");
console.log(`Loaded Campign: ${campaign}`);
//Load Months into Array for easy name refrence
const monthName = Object.keys(monthData);
const monthLength = Object.values(monthData);
const YearLength = monthData.length;

console.log(startDate, monthName, holidays, monthLength)
const campaignPath = `src/calendars/${campaign}.json`;

//Loads Holidays
console.log("Loading Holidays:");
holidays.forEach(element => 
	console.log(`\t${element.name}: ${suffixGen(element.day)} of ${Month(element.month)} (${element.month}/${element.day})`));
console.log("Loading Done!!");

// When the client is ready, run this code (only once)
client.once('ready', () => {
	console.log('Ready!');
	//console.log(token);
	console.log(`Logged in as ${client.user.tag}!`);
    console.log("Faerûn Calander system Kairos activated. Awaiting instruction.");
});

client.on('messageCreate', (message) => {
	console.log(`${message.author.tag}: ${message.content}`);
	if (message.content.startsWith(PREFIX) && !message.author.bot) {
		const [msg, ...args] = message.content
			.toLowerCase()
			.trim()
			.substring(PREFIX.length)
			.split(/\s+/);
		console.log(msg);
		console.log(args);

		//Command List
		switch(msg) {
			//Displays the Date
			case "date":
				message.reply(getDateString());
				break;
			case "dateshort":
				message.reply(getDate());
				break;
			//Adds time in terms of days (Months, Days)
			case "addtime":
				addTime(message, args);
				break;
			//Sets the Date
			case "setdate":
				setDate(message, args);
				break;
			//Adds an event
			case "addevent":
				break;
			case "dev":
				if (true) debug(message, args);
				else message.reply("Dev Mode is Disabled");
				break;
			default:
				return;
		}
	}
	else return;
});

//Month Utilities
function Month(num = 0) {
	return monthName[parseInt(num)-1];
}

function MonthLength(num = 0) {
	return monthLength[parseInt(num)-1];
}

//Returns the Current Date in short format
function getDate(){
	let {currentDate} = readJSON(campaignPath);
	//theDate = readJSON(campaignPath).currentDate;
	return `${currentDate.day}/${currentDate.month}/${currentDate.year}`
}

//Returns the Current Date as a String
function getDateString(){
	let {currentDate} = readJSON(campaignPath);
	let daySuffix = suffixGen(currentDate.day);

	let str = `${daySuffix} day of ${Month(currentDate.month)}, of the year ${currentDate.year}`;
	return str;
}

//Does calculations for the date.
function modifyDay(currentDate = {}, mod = 0) {
	let sum = parseInt(currentDate.day) + parseInt(mod);
	let extraDays = sum % MonthLength(parseInt(currentDate.month));
	let monthsPassed = Math.floor(sum / MonthLength(parseInt(currentDate.month)));
	console.log(`Calculating Days passing....
	Total Days: ${sum},
	Extra Days: ${extraDays},
	Months Elp: ${monthsPassed}`);

	//Changes the Day
	if (sum > MonthLength(parseInt(currentDate.month))) {
		currentDate = modifyMonth(currentDate, monthsPassed);

		//Deals with shifts between months with diffrent days
		if (monthsPassed > 1) {
			for (let i = 0; i < monthsPassed; i++) {
				let nextMonth = parseInt(currentDate.month) + i + 1;
				if (nextMonth > 12) nextMonth = 1;
				let dayDiffrence = MonthLength(parseInt(currentDate.month) + i) - MonthLength(nextMonth);
				extraDays -= Math.abs(dayDiffrence);
			}
		}
		currentDate.day = extraDays;
	}
	else currentDate.day = sum;
	//Return Date
	return currentDate;
}

//Changes the month and year if excededs
function modifyMonth(currentDate = {}, mod = 0) {
	let sum = parseInt(currentDate.month) + parseInt(mod);
	let extraYears = sum % YearLength;
	if (sum > YearLength) {
		currentDate.month = sum - (YearLength * extraYears);
		currentDate.year += extraYears;
	}
	else currentDate.month = sum;

	console.log(sum, extraYears);
	return currentDate;
}

//Handles command for adding time.
function addTime(msg, args = []){
	if (args.length === 0) return msg.reply("Invalid Arguments");
	let JsonCalander = readJSON(campaignPath);
	let {currentDate} = JsonCalander;
	
	if (args.length === 1) currentDate = modifyDay(currentDate, args[0]);
	else {
		for (let i = 0; i < args.length; i++) {
			switch(args[i]) {
				case "-d":
					currentDate = modifyDay(currentDate, args[i+1]);
					i++;
					break;
				case "-m":
					currentDate = modifyMonth(currentDate, args[i+1]);
					i++;
					break;
				case "-y":
					currentDate.year += parseInt(args[i+1]);
					i++;
					break;	
				default:
					break;
			}
		}
	}
	JsonCalander.currentDate = currentDate;
	writeJSON(campaignPath, JsonCalander);
	console.log(getDateString());
	msg.reply(getDateString());
}

function setDate(msg, args = []){
	console.log("Setting the date...")
	if (args.length > 3) return msg.reply("To Many Arguments!");
	let JsonCalander = readJSON(campaignPath);
	let sdDay = parseInt(args[0]);
	let sdMonth = parseInt(args[1]);
	let sdYear = parseInt(args[2]);
	
	//Sets the Dates
	JsonCalander.currentDate.day = sdDay;
	if (sdMonth != null && sdMonth <= YearLength) JsonCalander.currentDate.month = sdMonth;
	else { 
		return msg.reply(`Month can't supparse ${YearLength}!`); 
	}
	if (sdYear != null) JsonCalander.currentDate.year = sdYear;

	writeJSON(campaignPath, JsonCalander);
	//console.log(getDateString());
	msg.reply(getDateString());
}


//Utility
function debug(msg, args = []){

}

function readJSON(filename = ''){
		let rawdata = fs.readFileSync(filename, 'utf8') 
		let content = JSON.parse(rawdata);
		console.log(content);
		return content;
}

function writeJSON(filename = '', content){
	try {
		fs.writeFileSync(filename, JSON.stringify(content, null, 4));
		console.log (`Settings Saved`);
	} catch (err) {
		console.error(err);
	}
}

function suffixGen(num){
	const int = parseInt(num),
    digits = [int % 10, int % 100],
    ordinals = ['st', 'nd', 'rd', 'th'],
    oPattern = [1, 2, 3, 4],
    tPattern = [11, 12, 13, 14, 15, 16, 17, 18, 19];
  return oPattern.includes(digits[0]) && !tPattern.includes(digits[1])
    ? int + ordinals[digits[0] - 1]
    : int + ordinals[3];
}

client.login(token);
